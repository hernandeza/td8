import utile.Ut;

public class Voiture {

    // Attributs à définir
    private String unNom;
    private int uneVitesse;
    private int position;
    /**
     * Pré-requis : (à compléter)
     * Action : crée une voiture de nom unNom et de vitesse uneVitesse
     * placée à l’origine
     */
    public Voiture(String unNom, int uneVitesse , int position) {
        this.unNom = unNom;
        this.uneVitesse = uneVitesse;
        this.position = position ;
    }

    /**
     * Résultat : retourne une chaîne de caractères contenant les caractéristiques
     * de this (sous la forme de votre choix)
     */
    public String toString() {
        return this.unNom + " " + this.uneVitesse;
    }

    /**
     * Résultat : retourne une chaîne de caractères formée d’une suite d’espaces
     * suivie de la première lettre du nom de this, suivie d’un retour
     * à la ligne, le nombre d’espaces étant égal à la position de this.
     */

    public String toString2() {
        String chaine = "";

        for(int i = 0 ; i<=this.position ; i++){
            chaine += " ";
        }


        return chaine + "\n" + this.unNom.charAt(0);
    }

    /**
     * Résultat : retourne le nom de this
     */
    public String leNom() {
        return this.unNom;
    }

    /**
     * Résultat : retourne vrai si et seulement si la position de this est
     * supérieure ou égale à limite
     */
    public boolean depasse (int limite) {
        return this.position >= limite;
    }

    /**
     * Pré-requis : (à compléter)
     * Action : fait avancer this d’une distance égale à sa vitesse
     */

    public void avance() {
        this.position += this.uneVitesse;
    }

    /**
     * Action : place this au départ de la course (à l’origine)
     */
    public void auDepart() {
    this.position = 0;
    }


}

