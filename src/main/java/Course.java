import utile.Ut;

public class Course {

    private int longueur;
    private Voiture uneVoit1;
    private Voiture uneVoit2;

    /**
     * Pré-requis : (à compléter)
     * Action : (à compléter)
     */
    public Course(Voiture uneVoit1, Voiture uneVoit2, int longueur) {
    this.longueur = longueur;
    this.uneVoit1 = uneVoit1;
    this.uneVoit2 = uneVoit2;
    }

    /**
     * Résultat : retourne une chaîne de caractères contenant les caractéristiques
     * de this (sous la forme de votre choix)
     */
    public String toString() {
        return " " + this.longueur + " " + this.uneVoit1 + " " + this.uneVoit2;
    }

    /**
     * Action : Simule le déroulement d’une course entre this.voit1 et this.voit2
     * sur une piste de longueur this.longueurPiste.
     * this.voit1 et this.voit2 sont d’abord placées sur la ligne de départ.
     * <p>
     * Ensuite, jusqu’à ce qu’une voiture franchisse la ligne d’arrivée, l’une
     * des deux voitures est itérativement sélectionnée aléatoirement et avance.
     * Un affichage des deux voitures (représentées par la première lettre de leur
     * nom) à leur position respective à chaque étape permet de suivre la course.
     * <p>
     * Résultat : la voiture gagnante.
     */
    public Voiture deroulement() {
        this.uneVoit1.auDepart();
        this.uneVoit2.auDepart();


        int numVoit = 0;
        while(!this.uneVoit1.depasse(this.longueur) && !this.uneVoit2.depasse(this.longueur)) {
               numVoit =  Ut.randomMinMax(1 , 2);
            if(numVoit == 1){
                this.uneVoit1.avance();
                System.out.println(this.uneVoit1.toString2());
            }else {
                this.uneVoit2.avance();
                System.out.println(this.uneVoit2.toString2());
            }

        }
        if(this.uneVoit1.depasse(this.longueur)){
            return this.uneVoit1;
        }else{
            return this.uneVoit2;
        }
    }

}
